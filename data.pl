%% Database %%


:- use_module(library(csv)).
:- dynamic task_vacancy/2, person_preferences/2, csv_separator/1.

/*
 * Format:
 */
%person_preferences(joana, [dormir, comer, correr]).
%person_preferences(joao,  [correr, dormir, comer]).

%task_vacancy(dormir, 1).
%task_vacancy(correr, 1).
%task_vacancy(comer, 0).

clean_db :-
    forall(
        task_vacancy(X,Y),
        retract(task_vacancy(X,Y))
    ),
    forall(
        person_preferences(X,Y),
        retract(person_preferences(X,Y))
    ).


list_person_preferences(L) :-
    findall([N,P], person_preferences(N,P), L).

list_task_vacancy(L) :-
    findall([N,V], (task_vacancy(N,V), V > 0), L).


/*
 * read csv file, sane version:
 * use_module(library(csv)).
 * csv_read_file_row("ficheiro.csv", X, [line(Y)]),
 *     X =.. [row, Name | Prefs], assertz(person_preferences(Name, Prefs)).
 *
 * A more elaborate version: using DCGs, can use for both parsing and
 * outputing
 */

% Necessary evils (Excel and CSV interop)
% Default separators, changed if needed.
csv_separator(0',). %coma
csv_separator(0';). %semicollon
csv_separator(9).   %tab

% Test known separators in a csv file where we know the number of collumns
% on the first row
test_csv_separator(File, Arity) :-
    repeat,
        csv_separator(S),
        ( csv_read_file_row(File,R,[separator(S),functor(test)]),
          ( (functor(R,test,X), X < Arity)
            -> retract(csv_separator(S)), fail
             ; !
          )
        ), !.

process_csv_rows(File, Predicate) :-
    forall(
        (csv_separator(S),!,
         csv_read_file_row(File,Row,[convert(true),strip(true),separator(S)])),
        call(Predicate,Row)
    ).

process_person_preferences_row(Row) :-
    Row =.. [row,Name | Prefs],
    assertz(person_preferences(Name,Prefs)).

process_task_vacancy_row(row(Task, Vacancies)) :-
    number(Vacancies),
    assertz(task_vacancy(Task, Vacancies)).


read_person_preferences(File) :-
    process_csv_rows(File, process_person_preferences_row).
read_task_vacancy(File) :-
    process_csv_rows(File, process_task_vacancy_row).


% Write to csv (stream)

write_csv_distribuition(OutStream, Distribuition) :-
    forall(
         member([Name,Task], Distribuition),
         csv_write_stream(OutStream, [row(Name, Task)],[])
    ).

write_csv_rpeople(OutStream, RPeople) :-
    forall(
        member(Name, RPeople),
        csv_write_stream(OutStream, [row(Name, "<no task>")], [])
    ).


write_csv_rtasks(OutStream, RTasks) :-
    forall(
        member([Task,Vacancies], RTasks),
        csv_write_stream(OutStream, [row(Task, Vacancies)],[])
    ).


show_distribuition(Output, Distribuition) :-
    sort(1, @=<, Distribuition, Sorted),
    forall(
        member([Name, Task], Sorted),
        format(Output,'~s~t~24+~t~4+~s~n', [Name, Task])
    ).

show_rpeople(Output, RPeople) :-
    sort(RPeople, Sorted),
    forall(
        member(Name, Sorted),
        format(Output, '~s~t~24+~t~4+<no task>~n',[Name])
    ).

show_rtasks(Output, RTasks) :-
    sort(1, @=<, RTasks, Sorted),
    forall(
        member([Task, N], Sorted),
        format(Output, '~s~t~24+~t~4+~d~n',[Task, N])
    ).

auto_show_all(Output, Distribuition, RPeople, RTasks) :-
    distribuition_points(Distribuition, Points),
    writeln("Distribuition:"),
    show_distribuition(Output, Distribuition),
    writeln("Remaining People:"),
    show_rpeople(Output, RPeople),
    writeln("Remaining Vacancies:"),
    show_rtasks(Output, RTasks),
    format(Output, 'Satisfaction: ~d~n', [Points]).


