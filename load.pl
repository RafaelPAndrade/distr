/* File: load.pl
   Purpose: Load necessary files to start distribuition.
*  this is NOT idiomatic Prolog, but a necessary evil. For pure Prolog, see
*  dist.pl
*/

:- [ dist,
     data,
     utils
   ].

get_input_files(Input, Output, PrefFile, TaskFile) :-
    ask_question_answer_accept(
        Input, Output,
        "Which (CSV) file has the people's preferences?",
        PrefFile,
        readable_file
    ),
    ask_question_answer_accept(
        Input, Output,
        "Which (CSV) file has the task/vacancy list?",
        TaskFile,
        readable_file
    ).

get_data(Input, Output) :-
    writeln("Cleaning in-memory data base..."),
    clean_db,!,
    get_input_files(Input, Output, PrefFile, TaskFile),
    writeln("Testing CSV separators"),
    test_csv_separator(TaskFile, 2),
    writeln(Output, "Reading people's preferences"),
    read_person_preferences(PrefFile),
    writeln(Output, "Reading tasks & vacancies"),
    read_task_vacancy(TaskFile).

save_result(Input, Output, Distribuition, RPeople, RTasks) :-
    ask_question_yes_no(Input, Output, "Save this distribuition?"),
    ask_question_answer_accept(Input, Output,
                               "Save in which file (will overwrite file)?",
                               File,
                               writeable_file
                              ),
    open(File, write, OutStream, []),
    csv_write_stream(OutStream, [row('Distribuition')],[]),
    write_csv_distribuition(OutStream, Distribuition),
    csv_write_stream(OutStream, [row("Remaining People")],[]),
    write_csv_rpeople(OutStream, RPeople),
    csv_write_stream(OutStream, [row("Remaining Tasks")],[]),
    write_csv_rtasks(OutStream, RTasks),
    close(OutStream).


go :- go(user_input, user_output, user_error).


go(Input, Output, _) :-
    writeln(Output, "Starting."),
    get_data(Input, Output),!,
    repeat,
        distribuition(Distribuition, RPeople, RTasks),
        ignore(auto_show_all(Output, Distribuition, RPeople, RTasks)),
        ignore(save_result(Input, Output, Distribuition, RPeople, RTasks)),
        \+ ask_question_yes_no(Input, Output, "Generate more distribuitions?"),
    !.


