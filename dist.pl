% take_task_vacancy/3
%
% Note: Inefficient (?)
%
% Last Vacancy
take_task_vacancy(Task, [[Task, 1] | Tasks], Tasks).
% There are more vacancies
take_task_vacancy(Task, [[Task, X] | Tasks], [[Task, Y] | Tasks]) :-
    X > 1,
    Y is X - 1.
% Search for vacancy
take_task_vacancy(Task, [NTask | Tasks], [NTask | NTasks]) :-
    take_task_vacancy(Task, Tasks, NTasks).

% Distributing task
% distribuition/5
% people, tasks, distribuition, remaining people, remaining tasks

% no more people
distribuition([], Tasks, [], [], Tasks):-!.
% no more tasks
distribuition(People, [], [], People_Names, []) :- !,
    maplist(nth0(0), People, People_Names).

% Vacancy availiable
distribuition(
              [[Name, [Task0 | _]] | People],
              Tasks,
              [[Name,Task0] | Dist],
              RPeople,
              RTasks
) :-
    take_task_vacancy(Task0, Tasks, NTasks),
    distribuition(People, NTasks, Dist, RPeople, RTasks).

% Vacancy not available (take_task_vacancy failed)
distribuition(
              [[Name, [_ | Tasks]] | People],
              Activities,
              Distribuition,
              RPeople,
              RTasks
) :-
    NPerson = [Name, Tasks],
    append(People, [NPerson], NPeople),
    distribuition(NPeople, Activities, Distribuition, RPeople, RTasks).


distribuition(Distribuition, RPeople, RTasks) :-
    list_person_preferences(Preferences),
    list_task_vacancy(Tasks),
    distribuition(Preferences, Tasks, Distribuition, RPeople, RTasks).



distribuition_points(Dist, Pontuacao) :-
    distribuition_aux_points(Dist, 0, Pontuacao).

distribuition_aux_points([], Ac, Ac).
distribuition_aux_points(
              [[Name,Task] | Dist],
              Ac,
              Pontuacao
) :-
    person_preferences(Name, Pref),
    nth0(Pos, Pref, Task),
    length(Pref, Max),
    NAc is Ac + Max - Pos,
    distribuition_aux_points(Dist, NAc, Pontuacao).
