# "Theory" behind this program

### TL; DR:
  - This program solves a variant of the stable marriage problem
  - It uses Gale-Shapley algorithm variant too.
  - It can't find the optimal solution - but gives "stable solutions"

More on the stable marriage problem:
     https://en.wikipedia.org/wiki/Stable_marriage_problem

----


## Definitions

This program tries to find a "distribuition" of "people" among various "tasks",
where:

  - Each "person" has a list of "tasks", in order of preference
  - Each "task" has a limited number of vacancies


A "distribuition" is defined as a (non injective) function of people to tasks.
Also, it is not guaranteed that all persons have tasks assigned, unless
everyone includes all possible tasks in their list of preferences, and the sum
of all the vacancies is at least as big as the number of people; under that
circumstances, it is guaranteed everyone will have a task assigned.


## How does it work?

As in the Gale-Shapley algorithm, there are "rounds". The first person tries
to fill a vacancy in their most-preferred task. If the task has a vacancy for
this person, the person takes the place. If not, then this person has to try to
fill a vacancy in their second-preferred task, in the second round. A round ends
when all the people have tried to go to their first task. When this happens.
Everyone that did not fill a vacancy in the first round must try to fill the
second-preferred task; the remaining people continue to the Nth round, in which
they try to fill a vacancy in their Nth-preferred task, until they reach their
list end, or they are accepted.


## Differences between the stable marriage problem and the task filling problem

In the stable marriage problem, both suitors (those who propose) and
reviewers (those who accept) have a list of preferences. In this version,
only suitors (people) have preferences. The reviewers (tasks) only criterion
for acceptance is vacancies available.

Because of this difference, the definition of stability is also different.

In stable marriage, it was loosely defined as having no suitor (man) A and
reviewer (woman) B such that A prefers B to his partner, and B prefering A to
her partner.

With this program, "stability" is defined as having no two people that would
like to trade tasks.

## Pitfalls of this algorithm

The solutions found depend heavily on the people's order, because of the "first
come, first served" nature of the algorithm.

Here is a example of conditions that might arise:
Alice and Bob had task T0 as their favorite, and filled all the vacancies.
Charles also had task T0 as their favorite, but no vacancy was left.
Then, he tried to go his second-favorite, T1, but it was also already full.
Charles settles for T2, his least favorite task.

But both Alice and Bob had T2 as their second-favorite - which means that it
would be "better" in the global sense to have Charles on T0 and Bob in T2.
This means that the distribuition found is sub-optimal.

~~~prolog
task_vacancy(t0, 2).
task_vacancy(t1, 0).
task_vacancy(t2, 1).

person_preferences("Alice", [t0, t2, t1]).
person_preferences("Bob", [t0, t2, t1]).
person_preferences("Charles", [t0, t1, t2]).
~~~