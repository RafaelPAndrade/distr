# Distr
Logical task distribuition


### What?
A **Prolog** predicate that tries to assign tasks to people, according
to people's preferences and vacancies available.

### How?
Export the data about people's preferences and tasks/vacancies in CSV files, and
then let the program generate valid distribuitions of people/tasks.
You can save any distribuition you like. If you re-run with the same data, the
results will be identical.

For more details, please read THEORY.md.

## Notes
The algorithm employed is very sensitive regarding the people order.

The principal predicates are in **dist.pl**. The rest is just support for
user interaction and file reading.

Although the best solution can come in the beginning, it is not guaranteed.
Read THEORY.md for more details.

Also, this program can do more than distribute people and tasks. For example,
it could simulate the process of matching candidates to universities, if you
order the students by grade.

This was only tested in SWI Prolog; it does not use any advanced features of
the implementation (I think).

---

## TODO/IDEAS:
- Define a DCG to read and write to files
